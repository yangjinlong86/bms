Beauty Management System
====================================

- Spring Boot with Maven and Spring IO Platform for dependency management
- Web application (WAR) packaging as well as self-contained JAR
- Thymeleaf with Java 8 Time (Java8TimeDialect)
- WebJars
- Selenium configuration included
- Maven Wrapper included
