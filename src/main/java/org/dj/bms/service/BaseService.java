package org.dj.bms.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @ClassName: BaseService
 * @Description: TODO
 * @author pufangfei@163.com
 * @date 2017年11月8日 下午11:18:26
 */

public abstract class BaseService {
	/**
	 * 日志
	 */
	protected final Log logger = LogFactory.getLog(getClass());
}
