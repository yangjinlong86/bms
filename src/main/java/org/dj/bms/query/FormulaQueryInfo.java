package org.dj.bms.query;

/**
 * @ClassName: FormulaQueryIno
 * @Description: TODO
 * @author pufangfei@163.com
 * @date 2017年11月19日 下午8:37:57
 */

public class FormulaQueryInfo extends QueryInfo {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
