package org.dj.bms.query;

/**
 * @ClassName: ProductQueryInfo
 * @Description: TODO
 * @author pufangfei@163.com
 * @date 2017年11月16日 下午10:38:59
 */

public class ProductQueryInfo extends QueryInfo {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
